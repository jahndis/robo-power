import { Levels } from './Levels';

function TitleState()
{
    Phaser.State.call(this);
}

TitleState.prototype = Object.create(Phaser.State.prototype);
TitleState.prototype.constructor = TitleState;

TitleState.prototype.init = function ()
{
    this.stage.backgroundColor = '#3b1c32';
};

TitleState.prototype.preload = function ()
{
    this.load.image('title_robot', 'assets/title_robot.png');
    this.load.audio('music', 'assets/music.ogg');
};

TitleState.prototype.create = function ()
{
    var textStyle = {
        font: 'monospace',
        fontSize: '60pt',
        fontWeight: 'bold',
        fill: '#ffcf9c',
    };


    this.add.audio('music').play("", 0, 1, true);
    this.titleRobot = this.add.image(
        this.game.width * 0.5, this.game.height * 0.8,
        'title_robot');
    this.titleRobot.anchor.x = 0.5;
    this.titleRobot.anchor.y = 0.75;
    this.titleRobot.angle = -10;
    this.titleRobotTween = this.add.tween(this.titleRobot);
    this.titleRobotTween.to({ angle: 10, }, 1000, 'Linear', true, 0, -1)
    this.titleRobotTween.yoyo(true);

    this.titleText = this.add.text(400, 100, "ROBO-POWER", textStyle);
    this.titleText.anchor.x = 0.5;
    this.titleText.anchor.y = 0.75;
    this.titleText.angle = 10;
    this.titleTextTween = this.add.tween(this.titleText);
    this.titleTextTween.to({ angle: -10, }, 900, 'Linear', true, 0, -1)
    this.titleTextTween.yoyo(true);

    this.input.onDown.add(function () {
        this.game.state.start('StoryState');
    }, this);
};

TitleState.prototype.update = function ()
{
};

export { TitleState };
