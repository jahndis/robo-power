function RechargeStation(game, x, y, group)
{
    Phaser.Sprite.call(this, game, x, y, 'recharge');

    game.physics.arcade.enable(this);

    this.anchor.y = 0.25;
    this.body.setSize(64, 48);

    group.add(this);
}

RechargeStation.prototype = Object.create(Phaser.Sprite.prototype);
RechargeStation.prototype.constructor = RechargeStation;

export { RechargeStation };
