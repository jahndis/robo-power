function StoryState()
{
    Phaser.State.call(this);
}

StoryState.prototype = Object.create(Phaser.State.prototype);
StoryState.prototype.constructor = StoryState;

StoryState.STORIES = [];

StoryState.STORIES[0] = {
    emotions: [
        'happy', 'normal', 'angry', 'normal', 'normal', 'sad', 'normal',
        'normal', 'happy'
    ],
    text: [
        'Oh hey there ho there!',
        'You the fella they sent in here to be fix\'n e\'rythin\'?',
        'Whadaya mean "Wadaha mean"??!! No one told yall?!',
        'Our power\'s been out fer days!!',
        'Folks are resorting the most despicable activities...',
        'Talking to each other... even wanderin\' outside... *shudder*',
        'We really need our POWER BLOCKS put back in our appliances',
        'Looks like yall got plenty of them robot critters there to help',
        'Well I\'ll leave yall to yer business!'
    ]
};

StoryState.STORIES[1] = {
    emotions: [
        'normal', 'happy', 'angry', 'normal', 'happy', 'angry'
    ],
    text: [
        'Well I\'ll be!',
        'That looked like just about the easiest thing I\'ve ever seen!!',
        'How much\'r they payin\' yall?!',
        'Prob\'ly not near as much as I make...',
        'Me?! I\'m the mayor of this here city!!',
        'Which is why yall need to obey me and get on back to work!'
    ]
};

StoryState.STORIES[2] = {
    emotions: [
        'angry', 'angry', 'normal', 'sad', 'normal', 'sad'
    ],
    text: [
        'Hmm...',
        'I\'m still not entirely sure we need yall...',
        'What?!',
        'NO! Don\'t go!',
        'I swear I was kidding!!',
        'Please I need my sweet sweet frappucchocolattes...'
    ]
};

StoryState.STORIES[3] = {
    emotions: [
        'normal', 'happy', 'angry', 'angry', 'normal', 'normal'
    ],
    text: [
        'Oh hey there!',
        'I think yer doin\' a pretty great job!',
        'Although, I noticed one\'a yer robot fellas eatin\' some power blocks',
        'As mayor of this town, I will not tollerate this!!',
        '...',
        'By the way, my hot water heater could use fixin\'...'
    ]
};

StoryState.STORIES[4] = {
    emotions: [
        'happy', 'normal', 'normal', 'happy', 'normal'
    ],
    text: [
        'Yer smarter than I thought!',
        'Granted, I didn\'t think much to begin with...',
        'But did yall see those colored dots on the robots?',
        'Pretty useful fer tellin\' them apart from each other!',
        'Don\'t ya think?'
    ]
};

StoryState.STORIES[5] = {
    emotions: [
        'normal', 'normal', 'happy', 'happy', 'angry', 'angry', 'bored', 'bored'
    ],
    text: [
        'Whoooeee!',
        'That was a tough one!',
        'I\'ve pulled together some dollarinos for ya too',
        'We started building RECHARGE STATIONS!',
        'Yer little robo-buddies shouldn\'t need to eat power blocks no more',
        '...',
        'What!? I did yall a favor!',
        'Sheesh...'
    ]
};

StoryState.STORIES[6] = {
    emotions: [
        'happy', 'normal', 'happy', 'happy', 'bored'
    ],
    text: [
        'Heh heh heh!',
        'Those recharge stations are preetttty nifty!!',
        'I do say, puttin\' those in might seal me the next election!',
        '...',
        'Yall really need to stop lookin\' at me like that...'
    ]
};

StoryState.STORIES[7] = {
    emotions: [
        'normal', 'normal', 'angry', 'normal', 'angry', 'sad'
    ],
    text: [
        'Uh... so...',
        'A thought just ocurred to me...',
        'Would it be at all possible for one o\' yer little robo-things...',
        'To just sit on a RECHARGE STATION...',
        'And run forever?!?!?!',
        '...I knew the apocolypse was comin\'...'
    ]
};

StoryState.STORIES[8] = {
    emotions: [
        'angry', 'normal', 'angry', 'normal', 'happy'
    ],
    text: [
        'Hold on now!!',
        'Are yall tellin\' me yall can use more than one robot at a time?!',
        'Hmmm.... I\'ve got my eye on yall...',
        'Can\'t be havin\' robots takin\' over the world now can we!',
        '\'Specially not since I got ma\' new fluffy pool slippers...',
    ]
};

StoryState.STORIES[9] = {
    emotions: [
        'normal', 'angry', 'bored', 'bored', 'normal'
    ],
    text: [
        'Some o\' the folks around town are expressing concerns...',
        '...regarding the robots?...',
        '...',
        'I know I know! Yall just doin\' yer job!',
        '*huff*'
    ]
};

StoryState.STORIES[10] = {
    emotions: [
        'happy', 'normal', 'normal', 'happy', 'sad', 'normal', 'happy'
    ],
    text: [
        'Hoooeeyowwwaaa!!',
        'I think I\'ve warmed up to yer little robo-pals!',
        'Watchin\' \'em team up to take on the power shortage...',
        'It\'s dadgum inspirin\'!',
        'Makes me wish I had all sortsa minions...',
        'Doin\' whatever I say...',
        'Makin\' me deeelishus breakfast toast...'
    ]
};

StoryState.STORIES[11] = {
    emotions: [
        'sad', 'bored', 'angry', 'angry', 'sad', 'sad'
    ],
    text: [
        'YOOOOooooooeeeeaawwwwwwwww!!',
        '...',
        'Don\'t jus\' stand there!',
        'The space time continuum is rippin\' apart at the seams!!',
        'TIME WARPS are tearin\' this town to swiss cheese!!',
        'YAAAAAaaaaaaaaauuuuuuuaaaaawwwwwwww!!'
    ]
};

StoryState.STORIES[12] = {
    emotions: [
        'angry', 'angry', 'normal', 'happy', 'angry', 'normal'
    ],
    text: [
        'Wait 2 seconds...',
        'Yall are tellin\' me these WARPS can be used to help...',
        '...power the city?',
        'I knew from the start this\'d be a boon for the city!',
        'And everyone else thought those WARPS would only ruin stuff...',
        'HUFFAW!!'
    ]
};

StoryState.STORIES[13] = {
    emotions: [
        'normal', 'happy', 'bored', 'bored', 'normal', 'normal', 'happy',
        'bored', 'bored', 'bored', 'happy', 'normal', 'bored'
    ],
    text: [
        'Well pardner, yall\'ve done a spectacular job!',
        'Yall\'ve done this town a great service!',
        'Unfortunately, we\'ve completely run out of POWER BLOCKS...',
        '...so...',
        'Yer services will no longer be needed!',
        'If yall wouldn\'t mind pushin\' out with yer robo-mabobs',
        'I\'d be much obliged...',
        '...',
        'What?',
        'Yall thought there would be a more satisfyin\' endin\'?',
        'Heh heh heh...',
        'Yup.',
        'Nope.'
    ]
};

StoryState.prototype.init = function ()
{
    this.stage.backgroundColor = '#3b1c32';
};

StoryState.prototype.preload = function ()
{
    this.load.spritesheet('dude', 'assets/dude.png', 311, 301);
    this.load.image('speech_bubble', 'assets/speech_bubble.png');
};

StoryState.prototype.create = function ()
{
    this.story = StoryState.STORIES[this.game.storyIndex];

    this.dude = this.add.image(0, 40, 'dude');
    this.emotions = [ 'normal', 'happy', 'bored', 'angry', 'sad' ];

    this.add.image(0, 340, 'speech_bubble');
    var textStyle = {
        font: 'monospace',
        fontSize: '18pt',
        fontWeight: 'bold',
        fill: '#3b1c32',
        wordWrap: true,
        wordWrapWidth: 550,
    };
    var clickTextStyle = {
        font: 'monospace',
        fontSize: '12pt',
        fontWeight: 'bold',
        fill: '#3b1c32',
        wordWrap: true,
        wordWrapWidth: 550,
    };

    this.textIndex = 0;
    this.text = this.add.text(200, 350, this.story.text[this.textIndex],
        textStyle);
    this.clickText = this.add.text(690, 400, "Click 🡆", clickTextStyle);
    this.clickText.visible = false;

    this.inputEnabled = false;
    this.game.time.events.add(1000, function () {
        this.inputEnabled = true;
        this.clickText.visible = true;
    }, this);

    this.game.input.onDown.add(function () {
        if (this.inputEnabled)
        {
            this.textIndex += 1;
            if (this.textIndex === this.story.text.length)
            {
                if (this.game.storyIndex === 13)
                {
                    this.game.storyIndex = 0;
                    this.game.levelIndex = 0;
                    this.game.state.start('TitleState');
                }
                else
                {
                    this.game.state.start('GameState');
                }
            }
            else
            {
                if (this.textIndex === this.story.text.length - 1)
                {
                    if (this.game.storyIndex === 13)
                    {
                        this.clickText.text = "End 🡆";
                    }
                    else
                    {
                        this.clickText.text = "Start! 🡆";
                    }
                }
                this.text.text = this.story.text[this.textIndex];
                this.dude.frame =
                    this.emotions.indexOf(this.story.emotions[this.textIndex]);
            }

            this.inputEnabled = false;
            this.clickText.visible = false;
            this.game.time.events.add(1000, function () {
                this.inputEnabled = true;
                this.clickText.visible = true;
            }, this);
        }
    }, this);
};

export { StoryState };
