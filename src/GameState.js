import { Action } from './Action';
import { ControlPanel } from './ControlPanel';
import { EnergyBlock } from './EnergyBlock';
import { Levels } from './Levels';
import { RechargeStation } from './RechargeStation';
import { Robot } from './Robot';
import { Warp } from './Warp';

function GameState()
{
    Phaser.State.call(this);

    this.grid = null;
    this.robots = null;
    this.controlPanelSlots = null;
    this.actions = null;

    this.playing = false;
    this.gameTime = 700;
    this.win = false;
}

GameState.prototype = Object.create(Phaser.State.prototype);
GameState.prototype.constructor = GameState;

GameState.prototype.init = function (level)
{
    this.level = level;
    this.physics.startSystem(Phaser.Physics.ARCADE);
    this.stage.backgroundColor = '#3b1c32';
};

GameState.prototype.preload = function ()
{
    this.load.image('background', 'assets/background.png');
    this.load.image('grid', 'assets/grid.png');
    this.load.spritesheet('robot1', 'assets/robot1.png', 64, 64);
    this.load.spritesheet('robot2', 'assets/robot2.png', 64, 64);
    this.load.spritesheet('robot3', 'assets/robot3.png', 64, 64);
    this.load.spritesheet('robot4', 'assets/robot4.png', 64, 64);
    this.load.spritesheet('robot5', 'assets/robot5.png', 64, 64);
    this.load.spritesheet('robot_icon', 'assets/robot_icon.png', 32, 32);
    this.load.spritesheet('control_grid', 'assets/control_grid.png', 32, 32);
    this.load.spritesheet('energy_marker', 'assets/energy_marker.png', 32, 32);
    this.load.spritesheet('play', 'assets/play.png', 160, 32);
    this.load.image('energy', 'assets/energy.png');
    this.load.image('recharge', 'assets/recharge.png');
    this.load.spritesheet('warp', 'assets/warp.png', 64, 64);
    this.load.spritesheet('goal', 'assets/goal.png', 64, 64);
    this.load.spritesheet('actions', 'assets/actions.png', 32, 32);
    this.load.image('action_pool', 'assets/action_pool.png');
};

GameState.prototype.create = function ()
{
    /*
    this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(
        this.winLevel, this);
        */

    this.win = false;
    this.playing = false;
    this.simCounter = 0;

    var textStyle = {
        font: 'monospace',
        fontSize: '14pt',
        fontWeight: 'bold',
        fill: '#ffcf9c',
        wordWrap: true,
        wordWrapWidth: 200,
        align: 'center',
    };
    var textStyle2 = {
        font: 'monospace',
        fontSize: '14pt',
        fontWeight: 'bold',
        fill: '#ffcf9c',
        wordWrap: true,
        wordWrapWidth: 150,
        align: 'center',
    };
    var playTextStyle = {
        font: 'monospace',
        fontSize: '14pt',
        fontWeight: 'bold',
        fill: '#3b1c32',
        align: 'center',
    };
    var winTextStyle = {
        font: 'monospace',
        fontSize: '60pt',
        fontWeight: 'bold',
        fill: '#a4d4b4',
        align: 'center',
        stroke: '#ca054d',
        strokeThickness: 20 
    };


    this.grid = this.add.image(0, 0, 'background');

    this.controlPanelText = this.add.text(610, 15, 'CONTROL PANEL', textStyle);
    this.availableCommandsText = this.add.text(15, 365,
        'AVAILABLE COMMANDS', textStyle2);

    this.playButton = this.add.button(610, 400, 'play', this.onPressPlay, this);
    this.playText = this.add.text(this.playButton.x + 28, this.playButton.y + 5,
        'EXECUTE', playTextStyle);

    this.grid = this.add.group(this.world, 'grid');
    this.rechargeStations = this.add.group(this.world, 'recharge_stations');
    this.warps = this.add.group(this.world, 'warps');
    this.robots = this.add.group(this.world, 'robots');
    this.energyBlocks = this.add.group(this.world, 'energy_blocks');
    this.controlPanels = this.add.group(this.world, 'control_panels');
    this.controlPanelSlots = this.add.group(this.world, 'control_panel_slots');
    this.actions = this.add.group(this.world, 'actions');

    this.actionPool = this.add.image(135, 340, 'action_pool');

    this.goal = this.add.sprite(-1000, -1000, 'goal');

    this.loadLevel();

    this.winText = this.add.text(400, 200, 'GREAT JOB!', winTextStyle);
    this.winText.anchor.set(0.5);
    this.winText.visible = false;
};

GameState.prototype.onPressPlay = function ()
{
    if (!this.win)
    {
        this.playing = !this.playing;

        if (this.playing)
        {
            this.actions.setAll('inputEnabled', false);
            this.playButton.frame = 1;
            this.playText.text = 'RESTART';
        }
        else
        {
            this.resetLevel();
            this.actions.setAll('inputEnabled', true);
            this.playButton.frame = 0;
            this.playText.text = 'EXECUTE';
        }
    }
};

GameState.prototype.loadLevel = function ()
{
    var level = Levels[this.game.levelIndex];
    var tile;
    var tileX = Math.floor(this.game.width * 0.35) - (level.width * 64 / 2);
    var tileY = Math.floor(this.game.height * 0.38) - (level.height * 48 / 2);
    for (var i = 0; i < level.map.length; i++)
    {
        tile = level.map.charAt(i);

        if (tile === '|')
        {
            tileX = Math.floor(this.game.width * 0.35) - (level.width * 64 / 2);
            tileY += 48;
        }
        else
        {
            if (tile !== ' ')
            {
                this.grid.create(tileX, tileY, 'grid');
            }
            if (parseInt(tile))
            {
                new Robot(this.game, tileX + 32, tileY, parseInt(tile) - 1,
                    level.robots[parseInt(tile) - 1], this.robots);
            }
            if (tile === 'E')
            {
                new EnergyBlock(this.game, tileX, tileY, this.energyBlocks);
            }
            if (tile === 'R')
            {
                new RechargeStation(this.game, tileX, tileY,
                    this.rechargeStations);
            }
            if (tile === 'W')
            {
                new Warp(this.game, tileX, tileY, this.warps);
            }
            if (tile === 'G')
            {
                this.goal.x = tileX;
                this.goal.y = tileY;
                this.goal.animations.add('goal', [1, 2, 3, 4, 5], 10, false);
                this.game.physics.arcade.enable(this.goal);
                this.goal.anchor.y = 0.25;
                this.goal.body.setSize(64, 48);
            }

            tileX += 64;
        }
    }
    
    for (var i = 0; i < this.robots.total; i++)
    {
        new ControlPanel(this.game, 620, 50 + (i * 50),
            this.robots.getAt(i), this.controlPanels, this.controlPanelSlots);
    }

    var action;
    var actionX = this.actionPool.x + 16;
    var actionY = this.actionPool.y + 16;
    for (var i = 0; i < level.actions.length; i++)
    {
        action = Action.ACTION_MAPPINGS[level.actions[i]];
        new Action(this.game, actionX, actionY, action, this.actions);

        actionX += 32;
        if (i % 12 === 11)
        {
            actionX = this.actionPool.x + 16;
            actionY += 32;
        }
    }
}

GameState.prototype.resetLevel = function (levelNumber)
{
    this.game.tweens.removeAll();
    this.robots.callAll('restart');
    this.controlPanels.callAll('restart');
    this.energyBlocks.callAll('restart');
    this.win = false;
}

GameState.prototype.update = function ()
{
    this.game.physics.arcade.overlap(this.actions, this.controlPanelSlots,
        this.handleActionSlot, null, this);

    if (this.playing)
    {
        this.game.physics.arcade.overlap(this.robots, this.energyBlocks,
            this.handleRobotEnergy, null, this);
        this.game.physics.arcade.overlap(this.robots, this.rechargeStations,
            this.handleRobotRechargeStation, null, this);
        this.game.physics.arcade.overlap(this.robots, this.warps,
            this.handleRobotWarp, null, this);
        this.game.physics.arcade.overlap(this.robots, this.robots,
            this.handleRobotRobot, null, this);
        this.game.physics.arcade.overlap(this.goal, this.robots,
            this.handleGoalRobot, null, this);

        if (this.robots.getAll('isReady', true).length !== 0)
        {
            this.controlPanels.forEach(function (controlPanel) {
                controlPanel.executeAction(this.grid, this.gameTime);
            }, this);
        }
    }
    else
    {
    }
};

GameState.prototype.handleActionSlot = function (action, controlPanelSlot)
{
    if (action.input.isDragged)
    {
        if (controlPanelSlot.getBounds().contains(this.input.x, this.input.y) &&
            controlPanelSlot.action === null)
        {
            action.x = controlPanelSlot.x;
            action.y = controlPanelSlot.y;
            action.placed = true;
            action.slot = controlPanelSlot;
        }
        else
        {
            action.placed = false;
            action.slot = null;
            if (controlPanelSlot.action === action)
            {
                controlPanelSlot.action = null;
            }
        }
    }
};

GameState.prototype.handleRobotEnergy = function (robot, energy)
{
    if (!robot.isMoving)
    {
       if (robot.canTakeEnergy)
       {
           robot.pickUpEnergy();
           energy.x = -1000;
           energy.y = -1000;
       }
    }
};

GameState.prototype.handleRobotRechargeStation = function (robot, station)
{
    robot.onRechargeStation = true;
    if (!robot.isMoving)
    {
       if (robot.canTakeRecharge)
        {
            robot.canTakeRecharge = false;
            robot.setEnergy(4);
        }
    }
};

GameState.prototype.handleRobotWarp = function (robot, warp)
{
    robot.onWarp = true;
    if (!robot.isMoving)
    {
        if (robot.canWarp)
        {
            robot.canWarp = false;
            for (var i = 0; i < 2; i++)
            {
                if (this.warps.getAt(i) !== warp)
                {
                    robot.x = this.warps.getAt(i).x + 32;
                    robot.y = this.warps.getAt(i).y;
                }
            }
        }
    }
};

GameState.prototype.handleRobotRobot = function (robot1, robot2)
{
    if (!robot1.isMoving && !robot2.isMoving)
    {
        if (robot1.canTakeEnergy || robot2.canTakeEnergy)
        {
            if (robot1.canTakeEnergy && robot2.holdingEnergy)
            {
                robot2.dropEnergy();
                robot1.pickUpEnergy();
            }
            else if (robot2.canTakeEnergy && robot1.holdingEnergy)
            {
                robot1.dropEnergy();
                robot2.pickUpEnergy();
            }

            robot1.canTakeEnergy = false;
            robot2.canTakeEnergy = false;
        }
    }
    else
    {
        if (robot1.discharging && robot2.canTakeDischarge)
        {
            if (robot2.energy === 0)
            {
                robot2.powerUp();
            }
            robot2.setEnergy(robot2.energy + 1);
            robot2.canTakeDischarge = false;
        }
        if (robot2.discharging && robot1.canTakeDischarge)
        {
            if (robot1.energy === 0)
            {
                robot1.powerUp();
            }
            robot1.setEnergy(robot1.energy + 1);
            robot1.canTakeDischarge = false;
        }
    }
};

GameState.prototype.handleGoalRobot = function (goal, robot)
{
    robot.onGoal = true;
    if (!robot.isMoving)
    {
        if (robot.holdingEnergy && !this.win)
        {
            robot.dropEnergy();
            this.win = true;
            this.goal.play('goal');
            console.log('winner!');
            this.game.time.events.add(1000, function () {
                this.winText.visible = true;
            }, this);
            this.game.time.events.add(3000, this.winLevel, this);
        }
    }
};

GameState.prototype.winLevel = function ()
{
    this.game.storyIndex += 1;
    this.game.levelIndex += 1;
    this.game.state.start('StoryState');
};

export { GameState };
