function ControlPanel(game, x, y, robot, group, slotsGroup)
{
    Phaser.Group.call(this, game);

    this.robot = robot;

    this.robot.onSetEnergy.add(function () {
        this.setEnergy();
    }, this);

    this.robotIcon = game.add.existing(this.robot.icon);
    this.robotIcon.x = x;
    this.robotIcon.y = y;

    this.actionIndex = 0;

    this.energyMarkers = game.add.group(this.world, 'energy_markers');
    this.slots = [];
    for (var i = 0; i < 4; i++)
    {
        var slot = new Phaser.Sprite(game, x + 32 + (i * 32), y,
            'control_grid', 0);
        slot.inputEnabled = true;
        slot.controlPanel = this;
        slot.index = i;
        slot.action = null;
        game.physics.arcade.enable(slot);

        this.slots.push(slot);
        slotsGroup.add(slot);

        this.energyMarkers.create(x - 34, y + 2 - (i * 8), 'energy_marker');
    }

    this.setEnergy();

    group.add(this);
}

ControlPanel.prototype = Object.create(Phaser.Group.prototype);
ControlPanel.prototype.constructor = ControlPanel;

ControlPanel.prototype.restart = function ()
{
    this.setEnergy(this.robot.startEnergy);
    this.actionIndex = 0;
    for (var i = 0; i < 4; i++)
    {
        this.slots[i].frame = 0;
    }
};

ControlPanel.prototype.executeAction = function (grid, time)
{
    var action = this.slots[this.actionIndex].action;
    for (var i = 0; i < 4; i++)
    {
        if (i === this.actionIndex)
        {
            this.slots[i].frame = 1;
        }
        else
        {
            this.slots[i].frame = 0;
        }
    }

    if (this.robot.energy === 0)
    {
        this.robot.doAction({ type: 'nothing' }, false, grid, time);
    }
    else
    {
        if (action === null)
        {
            this.robot.doAction({ type: 'nothing' }, false, grid, time);
        }
        else
        {
            this.robot.doAction(action, true, grid, time);
        }

        this.actionIndex += 1;
        if (this.actionIndex === 4)
        {
            this.actionIndex = 0;
        }
    }
};

ControlPanel.prototype.setEnergy = function ()
{
    for(var i = 0; i < 4; i++)
    {
        if (i < this.robot.energy)
        {
            this.energyMarkers.getAt(i).frame = 1;
        }
        else
        {
            this.energyMarkers.getAt(i).frame = 0;
        }
    }
};

export { ControlPanel };
