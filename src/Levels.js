var Levels = [];

Levels[0] = {
width: 6,
height: 3,
map:
'......|' +
'.1E.G.|' +
'......|',
robots: [
    { energy: 4 },
],
actions: [
    'right',
    'right',
]
}

Levels[1] = {
width: 5,
height: 3,
map:
'.1...|' +
'..E..|' +
'...G.|',
robots: [
    { energy: 4 },
],
actions: [
    'right',
    'down',
    'up',
    'left',
]
}

Levels[2] = {
width: 8,
height: 3,
map:
' ...... |' +
'1.E..E.G|' +
' ...... |',
robots: [
    { energy: 3 },
],
actions: [
    'right',
    'right',
    'right',
    'right',
    'left',
    'left',
    'up',
    'down',
]
}

Levels[3] = {
width: 5,
height: 3,
map:
'1....|' +
'E....|' +
'.....|' +
'...EG|',
robots: [
    { energy: 3 },
],
actions: [
    'down',
    'down',
    'right',
    'right',
    'up',
    'left'
]
}

Levels[4] = {
width: 5,
height: 4,
map:
'..1E.|' +
' ..E.|' +
' E ..|' +
'.EG..|',
robots: [
    { energy: 4 },
],
actions: [
    'right',
    'down',
    'down',
    'up',
    'left',
    'left'
]
}

Levels[5] = {
width: 7,
height: 2,
map:
'E......|' +
'1 R R G|',
robots: [
    { energy: 4 },
],
actions: [
    'right',
    'right',
    'down',
    'up',
]
}

Levels[6] = {
width: 5,
height: 4,
map:
'.1   |' +
'..R  |' +
'...R |' +
'..E.G |',
robots: [
    { energy: 2 },
],
actions: [
    'right',
    'right',
    'down',
    'down',
    'up',
    'left',
    'left'
]
}

Levels[7] = {
width: 3,
height: 4,
map:
'2RG|' +
'.  |' +
'E  |' +
'1  |',
robots: [
    { energy: 4 },
    { energy: 1 },
],
actions: [
    'up',
    'right',
    'right',
    'left',
    'left'
]
}

Levels[8] = {
width: 6,
height: 3,
map:
'1E...G|' +
'   .  |' +
'   2  |',
robots: [
    { energy: 4 },
    { energy: 3 },
],
actions: [
    'up',
    'up',
    'right',
    'right',
    'right',
    'discharge'
]
}

Levels[9] = {
width: 4,
height: 3,
map:
' .E1|' +
'G...|' +
' 2. |',
robots: [
    { energy: 3 },
    { energy: 2 },
],
actions: [
    'up',
    'down',
    'down',
    'left',
    'left',
    'right',
    'discharge',
]
}

Levels[10] = {
width: 7, height: 4,
map:
'G.R5.R4|' +
' ......|' +
' .....R|' +
' 1E2R.3|',
robots: [
    { energy: 4 },
    { energy: 0 },
    { energy: 0 },
    { energy: 0 },
    { energy: 0 },
],
actions: [
    'right',
    'right',
    'right',
    'right',
    'right',
    'up',
    'up',
    'left',
    'left',
    'left',
    'left',
    'discharge',
    'discharge',
    'discharge',
    'discharge',
],
}

Levels[11] = {
width: 7, height: 3,
map:
'... ...|' +
'GW1 .WE|' +
'... ...|',
robots: [
    { energy: 4 },
],
actions: [
    'right',
    'left',
    'left',
    'left',
],
};

Levels[12] = {
width: 7, height: 6,
map:
'G  W   |' +
'...RE..|' +
'...1...|' +
'R. R E2|' +
'4.R3...|' +
'   W   |',
robots: [
    { energy: 4 },
    { energy: 4 },
    { energy: 0 },
    { energy: 0 },
],
actions: [
    'right',
    'left',
    'left',
    'left',
    'left',
    'left',
    'down',
    'down',
    'down',
    'up',
    'up',
    'up',
    'up',
    'up',
    'up',
    'discharge',
    'discharge',
],
};

export { Levels };
