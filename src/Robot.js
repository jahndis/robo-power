function Robot(game, x, y, index, data, group)
{
    Phaser.Sprite.call(this, game, x, y, 'robot' + (index + 1), 0);

    game.physics.arcade.enable(this);
    
    this.index = index;

    this.anchor.x = 0.5;
    this.anchor.y = 0.25;
    this.body.setSize(64, 48);
    this.scale.x = game.rnd.pick([-1, 1]);

    this.icon = new Phaser.Sprite(game, 0, 0, 'robot_icon', index);

    this.animations.add('idle', [0, 1], 10, true);
    this.animations.add('off', [2], 0, true);
    this.animations.add('move', [3, 4, 5], 10, true);
    this.animations.add('discharge', [6, 7, 8, 9], 10, true);
    this.animations.add('idle_cube', [10, 11], 10, true);
    this.animations.add('off_cube', [12], 0, true);
    this.animations.add('move_cube', [13, 14, 15], 10, true);

    group.add(this);

    this.onSetEnergy = new Phaser.Signal();

    this.startX = this.x;
    this.startY = this.y;
    this.startEnergy = data.energy;

    this.isReady = true;
    this.isMoving = false;
    this.energy = 0;
    this.setEnergy(data.energy);
    this.holdingEnergy = false;
    this.canTakeEnergy = true;
    this.canTakeDischarge = false;
    this.discharging = false;
    this.canTakeRecharge = false;
    this.onRechargeStation = false;
    this.canWarp = false;
    this.onWarp = false;
    this.onGoal = false;

    if (this.energy === 0)
    {
        this.play('off');
    }
    else
    {
        this.play('idle');
    }

}

Robot.prototype = Object.create(Phaser.Sprite.prototype);
Robot.prototype.constructor = Robot;

Robot.prototype.setEnergy = function (energy)
{
    this.energy = energy;
    this.onSetEnergy.dispatch();
};

Robot.prototype.restart = function ()
{
    this.x = this.startX;
    this.y = this.startY;
    this.setEnergy(this.startEnergy);
    this.icon.frame = this.index;
    this.isReady = true;
    this.isMoving = false;
    this.holdingEnergy = false;
    this.canTakeEnergy = true;
    this.canTakeDischarge = true;
    this.discharging = false;
    this.canTakeRecharge = true;
    this.onRechargeStation = false;
    this.canWarp = true;
    this.onWarp = false;
    this.onGoal = false;

    if (this.energy === 0)
    {
        this.play('off');
    }
    else
    {
        this.play('idle');
    }
};

Robot.prototype.doAction = function (action, loseEnergy, grid, time)
{
    var tween;

    if (action.type === 'nothing')
    {
        tween = this.game.add.tween(this).to({ x: this.x }, time, 'Linear');
    }

    if (action.type === 'move')
    {
        if (this.holdingEnergy)
        {
            this.play('move_cube');
        }
        else
        {
            this.play('move');
        }
        if (action.moveX > 0)
        {
            this.scale.x = -1;
        }
        else if (action.moveX < 0)
        {
            this.scale.x = 1;
        }

        var validMove = false;
        var moveToX = this.x + action.moveX;
        var moveToY = this.y + action.moveY;
        grid.forEach(function (gridTile) {
            if (gridTile.getBounds().contains(moveToX, moveToY))
            {
                validMove = true;
            }
        }, this);

        if (validMove)
        {
            tween = this.game.add.tween(this).to({ x: moveToX, y: moveToY, },
                time, 'Linear');
        }
        else
        {
            tween = this.game.add.tween(this).to({ x: this.x }, time, 'Linear');
        }
    }

    if (action.type === 'discharge')
    {
        this.play('discharge');
        tween = this.game.add.tween(this).to({ x: this.x }, time, 'Linear');
        this.discharging = true;
    }

    tween.onComplete.add(function () {
        if (this.holdingEnergy)
        {
            this.play('idle_cube');
        }
        else
        {
            this.play('idle');
        }
        if (this.energy === 0)
        {
            if (this.holdingEnergy && !this.onGoal && !this.onRechargeStation)
            {
                this.useEnergy()
            }
            else if (this.onRechargeStation)
            {
                // Don't power down
            }
            else
            {
                this.powerDown();
            }
        }

        this.isMoving = false;
        this.discharging = false;
        this.canTakeDischarge = true;
        this.canTakeRecharge = true;
        this.onRechargeStation = false;
        this.canWarp = true;
        this.onWarp = false;

        this.game.time.events.add(time, function () {
            this.isReady = true;
        }, this);
    }, this);
    tween.start();

    this.isReady = false;
    this.isMoving = true;
    this.canTakeEnergy = !this.holdingEnergy;

    if (loseEnergy)
    {
        this.setEnergy(this.energy - 1);
    }
};

Robot.prototype.pickUpEnergy = function ()
{
    this.icon.frame = this.index + 5;
    this.play('idle_cube');
    this.holdingEnergy = true;
    this.canTakeEnergy = false;
};

Robot.prototype.dropEnergy = function ()
{
    this.icon.frame = this.index;
    if (this.energy === 0)
    {
        // Hmm... this.play('idle_cube');
        this.play('idle');
    }
    else
    {
        this.play('idle');
    }
    this.holdingEnergy = false;
    this.canTakeEnergy = true;
};

Robot.prototype.useEnergy = function ()
{
    this.icon.frame = this.index;
    this.play('idle');
    this.holdingEnergy = false;
    this.canTakeEnergy = true;
    this.setEnergy(4);
};

Robot.prototype.powerUp = function ()
{
    if (this.holdingEnergy)
    {
        this.canTakeEnergy = false;
        this.play('idle_cube');
    }
    else
    {
        this.canTakeEnergy = true;
        this.play('idle');
    }
};

Robot.prototype.powerDown = function ()
{
    this.canTakeEnergy = false;
    if (this.holdingEnergy)
    {
        this.play('off_cube');
    }
    else
    {
        this.play('off');
    }
};

export { Robot };
