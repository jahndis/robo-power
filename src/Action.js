function Action(game, x, y, data, group)
{
    Phaser.Sprite.call(this, game, x, y, 'actions', data.sprite);

    game.physics.arcade.enable(this);

    this.startX = x;
    this.startY = y;

    this.inputEnabled = true;
    this.input.enableDrag();
    this.events.onDragStart.add(this.onDragStart, this);
    this.events.onDragStop.add(this.onDragStop, this);

    this.placed = false;
    this.slot = null;

    this.type = data.type;
    this.moveX = data.moveX;
    this.moveY = data.moveY;
    
    group.add(this);
}

Action.ACTION_MAPPINGS = {
    up: { type: 'move', sprite: 0, moveX: 0, moveY: -48 },
    right: { type: 'move', sprite: 1, moveX: 64, moveY: 0 },
    down: { type: 'move', sprite: 2, moveX: 0, moveY: 48 },
    left: { type: 'move', sprite: 3, moveX: -64, moveY: 0 },
    discharge: { type: 'discharge', sprite: 4 },
};

Action.prototype = Object.create(Phaser.Sprite.prototype);
Action.prototype.constructor = Action;

Action.prototype.onDragStart = function ()
{
};

Action.prototype.onDragStop = function ()
{
    if (this.placed)
    {
        this.x = this.slot.x;
        this.y = this.slot.y;
        this.slot.action = this;
    }
    else
    {
        this.x = this.startX;
        this.y = this.startY;
    }
};

export { Action };
