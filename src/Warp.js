function Warp(game, x, y, group)
{
    Phaser.Sprite.call(this, game, x, y, 'warp');

    game.physics.arcade.enable(this);

    this.animations.add('warp', [0, 1, 2, 3, 4], 10, true);
    this.play('warp');

    this.anchor.y = 0.25;
    this.body.setSize(64, 48);

    group.add(this);
}

Warp.prototype = Object.create(Phaser.Sprite.prototype);
Warp.prototype.constructor = Warp;

export { Warp };
