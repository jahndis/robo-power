function EnergyBlock(game, x, y, group)
{
    Phaser.Sprite.call(this, game, x, y, 'energy');

    game.physics.arcade.enable(this);

    this.anchor.y = 0.25;
    this.body.setSize(64, 48);

    group.add(this);

    this.startX = x;
    this.startY = y;
}

EnergyBlock.prototype = Object.create(Phaser.Sprite.prototype);
EnergyBlock.prototype.constructor = EnergyBlock;

EnergyBlock.prototype.restart = function ()
{
    this.x = this.startX;
    this.y = this.startY;
};

export { EnergyBlock };
