import { GameState } from './GameState';
import { StoryState } from './StoryState';
import { TitleState } from './TitleState';

var game = new Phaser.Game(800, 450, Phaser.AUTO);

game.state.add('TitleState', TitleState);
game.state.add('StoryState', StoryState);
game.state.add('GameState', GameState);

game.storyIndex = 0;
game.levelIndex = 0;

game.state.start('TitleState');
//game.state.start('StoryState');
//game.state.start('GameState');
