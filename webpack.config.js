module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'ld39.js'
    },
    devServer: {
        hot: false,
        inline: true
    }
};
